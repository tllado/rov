#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <linux/input.h>
#include <linux/joystick.h>

double Vvel(double, double);
double Hvel(double, double, double, int);
double Cpos(double, double, double);
double checkdir(double, double);
int duty(double);
int direction(double);


int main (void)
{
	int fd = 0;
	int i = 8;
	int gamepad[27][2] = {0};
	double vel[6] = {0.0};
	struct js_event js;

	//initialize non-zero elements
	while (i < 20) {
		gamepad[i][0] = -32767;
		i++;
	}
	vel[5] = 0.5;

	//Open gamepad filestream
	if ((fd = open("/dev/input/js0", O_RDONLY)) < 0) {
		perror("jstest");
		return 1;
	}

	//Primary control loop
	while (1) {
		//Read gamepad data
		if (read(fd, &js, sizeof(struct js_event)) != sizeof(struct js_event)) {
			perror("\njstest: error reading");
			return 1;
		}

		//Assign new gamepad values
		if (js.type == 0 || 1)
			gamepad[js.number-1][js.type]=js.value;

		//Calculate thruster velocities
		if(js.number == 12 || js.number == 13)
			vel[0] = checkdir(vel[0], Vvel((gamepad[12][0] + 32767.0)/65534.0, (gamepad[13][0] + 32767.0)/65534.0));
		if(js.number == 1 || js.number == 2 || js.number == 3) {
			vel[1] = checkdir(vel[1], Hvel(-gamepad[1][0]/32767.0, gamepad[2][0]/32767.0, gamepad[3][0]/32767.0, 1));
			vel[2] = checkdir(vel[2], Hvel(-gamepad[1][0]/32767.0, gamepad[2][0]/32767.0, gamepad[3][0]/32767.0, 2));
			vel[3] = checkdir(vel[3], Hvel(-gamepad[1][0]/32767.0, gamepad[2][0]/32767.0, gamepad[3][0]/32767.0, 3));
			vel[4] = checkdir(vel[4], Hvel(-gamepad[1][0]/32767.0, gamepad[2][0]/32767.0, gamepad[3][0]/32767.0, 4));
		}

		//calculate camera servo position
		if(js.number == 14 || js.number == 15)
			vel[5] = Cpos((gamepad[14][0] + 32767.0)/65534.0, (gamepad[15][0] + 32767.0)/65534.0, vel[5]);

		//print motor speeds
		printf("\033[2J\033[50A");
		printf("Time: %d\n", js.time);
		printf("H1 vel: %d %d\tH2 vel: %d %d\n", duty(vel[1]), direction(vel[1]), duty(vel[2]), direction(vel[2]));
		printf("H3 vel: %d %d\tH4 vel: %d %d\n", duty(vel[3]), direction(vel[3]), duty(vel[4]), direction(vel[4]));
		printf("V Vel: %d %d\tCam Pos: %d\n", duty(vel[0]), direction(vel[0]), duty(vel[5]));

		//Write duty and direction values to Beaglebone

		fflush(stdout);
	}

	//End error
	printf("jstest: something didn't work");
	return -1;
}

//Calculate vertical thruster velocity
double Vvel(double down, double up) {
	if(up > down)
		return up;
	if(up > 0.0)
		return 0.0;
	return -down;
}

//Calculate horizontal thruster velocity
double Hvel(double fwdthrust, double horiturn, double vertturn, int number) {
	if(horiturn + vertturn > 1)
		return 0.5*fwdthrust + ((number%2 == 1?horiturn/2.0:-horiturn/2.0) + (number/3 == 1?vertturn/2.0:-vertturn/2.0))/(horiturn + vertturn);
	return fwdthrust*(1.0 - fabs(horiturn)/2.0 - fabs(vertturn)/2.0) + (number%2 == 1?horiturn/2.0:-horiturn/2.0) + (number/3 == 1?vertturn/2.0:-vertturn/2.0);
}

//Calculate camera servo position
//If up and down both == 1, camera resets to center
//If up and down both > 0 but not both == 1, camera doesn't move
double Cpos(double down, double up, double oldpos) {
	if(down > 0 && up > 0) {
		if(down == 1 && up == 1)
			return 0.5;
		return oldpos;
	}
	if(down > 0)
		return 1.0 - down < oldpos?1.0 - down:oldpos;
	return up > oldpos?up:oldpos;
}

//Prevents reverse current when switching motor direction
double checkdir(double oldvel, double newvel) {
	if(oldvel/fabs(oldvel) != newvel/fabs(newvel))
		return 0;
	return newvel;
}

//Convert velocity (-1 to 1) to duty (19-18mil nanoseconds)
int duty(double vel) {
	return 19000000 - 1000000*fabs(vel);
}

//Determine motor direction based on velocity
int direction(double vel) {
	if(vel >= 0.0)
		return 0;
	return 1;
}
