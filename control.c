#include <sys/ioctl.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>
#include <time.h>
#include <linux/input.h>
#include <linux/joystick.h>

double Vvel(double, double);
double Hvel(double, double, double, int);
double Cpos(double, double, int, double);
double checkdir(double, double);
int calcduty(double);
int calcdir(double);
void transmit(char *beagleIP, unsigned short beaglePort, int array, int location, int value);
void BroadcastSend(char *broadcastIP, unsigned short broadcastPort, char *sendString);

int main (void)
{
	//Declare and initialize
	//Defaults:
	//-all gamepad buttons at neutral values
	//-all motor in fwd direction, zero velocity
	//-camera in center position
	int i = 8;
	int fd = 0;
	int Creset = 0;
	int LmnDuty = 0;
	int Lstep = 0;
	int Lreset = 0;
	int LbuIO = 1;
	int gamepad[27][3] = {0};
	double vel[6] = {0.0};
	int duty[6] = {19000000};
	int direction[5] = {0};
	struct js_event js;
	while (i < 20) {
		gamepad[i][2] = -32767;
		i++;
	}
	vel[5] = 0.5;
	duty[5] = 18500000;
	int intchoice = 0;
	char beagleIP[16] = "169.254.250.78";
	unsigned short beaglePort = 8100;

	//Menu Screen
	printf("\033[2J\033[50A");
	printf("Sunfish Surface Control System v0.1\nJuly 2013\n\n");
	printf("ROV location?\n1) Tether\n2) Router\n3) Enter Other\n");
	fflush(stdout);
	scanf("%d", &intchoice);
	
	//Set beaglebone IP address
	if(intchoice == 1)
		strcpy(beagleIP, "169.254.250.78");
	else if(intchoice == 2)
		strcpy(beagleIP, "192.168.1.146");
	else {
		printf("Enter beagle's IP address\n");
		fflush(stdout);
		scanf("%s", beagleIP);
	}

	printf("\nWill connect to %s:8100\n\n", beagleIP);

	//Open filestream to gamepad
	if((fd = open("/dev/input/js0", O_RDONLY)) < 0) {
		
		perror("joystick not available");
		return 1;
	}

//////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////
	//Get the right gamepad value for the START button
	printf("\nPress START to operate Sunfish\n");
	while (gamepad[20/*START*/][1] == 0) {
		if (read(fd, &js, sizeof(struct js_event)) != sizeof(struct js_event)) {
			perror("\njoystick: error reading");
			return 1;
		}
	}

	//Start MJPG-Streamer
	//how do I do this?
	//is there a command I can put in here that will just execute it as if from the terminal?

	//Main control loop
	while (1) {
		//Read gamepad data
		if (read(fd, &js, sizeof(struct js_event)) != sizeof(struct js_event)) {
			perror("\njoystick: error reading");
			return 1;
		}

		//Assign new gamepad values
		if (js.type == 1 || js.type == 2)
			gamepad[js.number][js.type] = js.value;

		//Calculate thruster velocities
		if(js.number == 12 || js.number == 13) {
			vel[0] = checkdir(vel[0], Vvel((gamepad[12][2] + 32767.0)/65534.0, (gamepad[13][2] + 32767.0)/65534.0));
			duty[0] = calcduty(vel[0]);
			direction[0] = calcdir(vel[0]);
		}
		if(js.number == 1 || js.number == 2 || js.number == 3) {
			vel[1] = checkdir(vel[1], Hvel(-gamepad[1][2]/32767.0, gamepad[2][2]/32767.0, gamepad[3][2]/32767.0, 1));
			vel[2] = checkdir(vel[2], Hvel(-gamepad[1][2]/32767.0, gamepad[2][2]/32767.0, gamepad[3][2]/32767.0, 2));
			vel[3] = checkdir(vel[3], Hvel(-gamepad[1][2]/32767.0, gamepad[2][2]/32767.0, gamepad[3][2]/32767.0, 3));
			vel[4] = checkdir(vel[4], Hvel(-gamepad[1][2]/32767.0, gamepad[2][2]/32767.0, gamepad[3][2]/32767.0, 4));
			duty[1] = calcduty(vel[1]);
			duty[2] = calcduty(vel[2]);
			duty[3] = calcduty(vel[3]);
			duty[4] = calcduty(vel[4]);
			direction[1] = calcdir(vel[1]);
			direction[2] = calcdir(vel[2]);
			direction[3] = calcdir(vel[3]);
			direction[4] = calcdir(vel[4]);
		}

		//Calculate camera servo position
		if(js.number == 14 || js.number == 15) {
			if(gamepad[14][2] == 32767 && gamepad[15][2] == 32767 && Creset == 0)
				Creset = 1;
			else if(gamepad[14][2] == -32767 && gamepad[15][2] == -32767)
				Creset = 0;
			vel[5] = Cpos((gamepad[14][2] + 32767.0)/65534.0, (gamepad[15][2] + 32767.0)/65534.0, Creset, vel[5]);
			duty[5] = calcduty(vel[5]);
		}

		//Set LED brightness and state
		if(js.number == 16 || js.number == 19) {
			if(gamepad[16][2] == 32767 && gamepad[19][2] == 32767 && Lreset == 0) {
				Lreset = 1;
				Lstep = 1;
				LbuIO = (LbuIO == 0?1:0);
				LmnDuty = (LbuIO == 1?0:100000);
			}
			else if(gamepad[16][2] == -32767 && gamepad[19][2] == -32767) {
				Lreset = 0;
				Lstep = 0;
			}
			else if(gamepad[16][2] > -32767 && gamepad[19][2] > -32767)
				;
			else if(gamepad[16][2] == 32767 && LbuIO == 0)
				LmnDuty = 1000000;
			else if(gamepad[19][2] == 32767 && LbuIO == 0)
				LmnDuty = 100000;
			else if(gamepad[16][2] > -32767 && Lstep == 0 && LbuIO == 0) {
				LmnDuty = (LmnDuty >= 1000000?1000000:(LmnDuty += 100000));
				Lstep = 1;
			}
			else if(gamepad[19][2] > -32767 && Lstep == 0 && LbuIO == 0) {
				LmnDuty = (LmnDuty <= 0?0:(LmnDuty -= 100000));
				Lstep = 1;
			}
		}

		//Transmit new value
		//Set this up at the end of each calculation, much less redundant info and code

		//print motor speeds
		printf("\033[2J\033[50A");
		printf("Time: %d\n", js.time);
		printf("H1 vel: %d %d\tH2 vel: %d %d\n", duty[1], direction[1], duty[2], direction[2]);
		printf("H3 vel: %d %d\tH4 vel: %d %d\n", duty[3], direction[3], duty[4], direction[4]);
		printf("V Vel: %d %d\tCam Pos: %d\n", duty[0], direction[0], duty[5]);
		printf("M LED: %d\t B LED: %d\n", LmnDuty, LbuIO);

		fflush(stdout);
	}

	//End error
	printf("jstest: something didn't work");
	return -1;
}

//Calculate vertical thruster velocity
double Vvel(double down, double up) {
	if(up > down)
		return up;
	if(up > 0.0)
		return 0.0;
	return -down;
}

//Calculate horizontal thruster velocity
double Hvel(double fwdthrust, double horiturn, double vertturn, int number) {
	if(horiturn + vertturn > 1)
		return 0.5*fwdthrust + ((number%2 == 1?horiturn/2.0:-horiturn/2.0) + (number/3 == 1?vertturn/2.0:-vertturn/2.0))/(horiturn + vertturn);
	return fwdthrust*(1.0 - fabs(horiturn)/2.0 - fabs(vertturn)/2.0) + (number%2 == 1?horiturn/2.0:-horiturn/2.0) + (number/3 == 1?vertturn/2.0:-vertturn/2.0);
}

//Calculate camera servo position
double Cpos(double down, double up, int reset, double oldpos) {
	if(reset == 1)
		return 0.5;
	if(down > 0 && up > 0)
		return oldpos;
	if(down > 0)
		return 1.0 - down < oldpos?1.0 - down:oldpos;
	return up > oldpos?up:oldpos;
}

//Prevents current spikes when switching motor direction
double checkdir(double oldvel, double newvel) {
	if(((oldvel > 0) - (oldvel < 0))*((newvel > 0) - (newvel < 0)) == -1)
		return 0;
	return newvel;
}

//Convert velocity (-1 to 1) to duty (19-18mil nanoseconds)
int calcduty(double vel) {
	return 19000000 - 1000000*fabs(vel);
}

//Determine motor direction based on velocity
int calcdir(double vel) {
	if(vel >= 0.0)
		return 0;
	return 1;
}

//Concatenate data and send to beaglebone
void transmit(char *beagleIP, unsigned short beaglePort, int array, int location, int value)
{
	int messageValue = array*10000000000 + location*100000000 + value;
	char message[11];
	sprintf(message, "%d", messageValue);
	BroadcastSend(beagleIP, beaglePort, message);
	return;
}

//Send message over TCP broadcast
void BroadcastSend(char *broadcastIP, unsigned short broadcastPort, char *sendString)
{
	int sock;				/* Socket */
	struct sockaddr_in broadcastAddr;	/* Broadcast address */
	int broadcastPermission;		/* Socket opt to set permission to broadcast */
	unsigned int sendStringLen;		/* Length of string to broadcast */

	/* Create socket for sending/receiving datagrams */
	if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0) {
		printf("socket() failed");
		return;
	}

	/* Set socket to allow broadcast */
	broadcastPermission = 1;
	if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission, sizeof(broadcastPermission)) < 0) {
		printf("setsockopt() failed");
		return;
	}

	/* Construct local address structure */
	memset(&broadcastAddr, 0, sizeof(broadcastAddr));	/* Zero out structure */
	broadcastAddr.sin_family = AF_INET;			/* Internet address family */
	broadcastAddr.sin_addr.s_addr = inet_addr(broadcastIP);	/* Broadcast IP address */
	broadcastAddr.sin_port = htons(broadcastPort);		/* Broadcast port */

	sendStringLen = strlen(sendString);	/* Find length of sendString */
	for (;;) {				/* Run forever */
         /* Broadcast sendString in datagram to clients every 3 seconds*/
         if (sendto(sock, sendString, sendStringLen, 0, (struct sockaddr *)&broadcastAddr, sizeof(broadcastAddr)) != sendStringLen)
			printf("sendto() sent a different number of bytes than expected");

//		sleep(3);	/* Avoids flooding the network */
	}
	/* NOT REACHED */

	return;
}
