#include <stdio.h>      //for printf() and fprintf()
#include <sys/socket.h> //for socket(), connect(), sendto(), and recvfrom()
#include <arpa/inet.h>  //for sockaddr_in and inet_addr()
#include <stdlib.h>     //for atoi() and exit()
#include <string.h>     //for memset()
#include <unistd.h>     //for close()

#define MAXRECVSTRING 255  //Longest string to receive

//Function Declarations
void BroadcastSend(char *SendString);

//Laptop IP Address
char *LaptopIP = "192.168.1.141";
unsigned short LaptopPort = 8100;	//Port

//TCP variables
int SockIn;				//Socket
struct sockaddr_in BroadcastAddrIn;	//Broadcast Address
char recvString[MAXRECVSTRING+1];	//Buffer for received string
int recvStringLen;			//Length of received string
int SockOut;
struct sockaddr_in BroadcastAddrOut;	//Broadcast address
int broadcastPermission;		//Socket opt to set permission to broadcast
unsigned int SendStringLen;		//Length of string to broadcast

//Main
int main(void)
{
	//Control variables
	int message, x, y, z = {0};
	int pwm[8] = {19000};
	int gpio[8] = {0};

	//Assign non-zero default values
	pwm[5] = 18500;
	gpio[6] = 1;

//Setup Receive Socket***************************************************************************
	//Create a best-effort datagram socket using UDP
	if ((SockIn = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
		printf("socket() failed\n");

	//Construct bind structure
	memset(&BroadcastAddrIn, 0, sizeof(BroadcastAddrIn));	//Zero out structure
	BroadcastAddrIn.sin_family = AF_INET;			//Internet address family
	BroadcastAddrIn.sin_addr.s_addr = htonl(INADDR_ANY);	//Any incoming interface
	BroadcastAddrIn.sin_port = htons(LaptopPort);		//Broadcast port

	//Bind to the broadcast port
	if (bind(SockIn, (struct sockaddr *) &BroadcastAddrIn, sizeof(BroadcastAddrIn)) < 0)
		printf("bind() failed\n");

//Setup Send Socket*****************************************************************************
	//Create socket for sending/receiving datagrams
	if ((SockOut = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
		printf("socket() failed\n");

	//Set socket to allow broadcast
	broadcastPermission = 1;
	if (setsockopt(SockOut, SOL_SOCKET, SO_BROADCAST, (void *) &broadcastPermission, sizeof(broadcastPermission)) < 0)
		printf("setsockopt() failed\n");

	//Construct local address structure
	memset(&BroadcastAddrOut, 0, sizeof(BroadcastAddrOut));	//Zero out structure
	BroadcastAddrOut.sin_family = AF_INET;			//Internet address family
	BroadcastAddrOut.sin_addr.s_addr = inet_addr(LaptopIP);	//Broadcast IP address
	BroadcastAddrOut.sin_port = htons(LaptopPort);		//Broadcast port
//*********************************************************************************************:

	//Main control loop
	while(1) {
		//Receive a single datagram from the server
		if ((recvStringLen = recvfrom(SockIn, recvString, MAXRECVSTRING, 0, NULL, 0)) < 0)
			printf("recvfrom() failed\n");

		recvString[recvStringLen] = '\0';

		if(recvString == "heart")
			BroadcastSend("beat");
		else {
			message = strtol(recvString, NULL, 10);
			x = message/1000000;
			y = message%1000000/100000;
			z = message%100000;

			//assign received values to pin arrays
			if(x == 0)
				pwm[y] = z;
			else if(x == 1)
				gpio[y] = z;

			//Assign values to pins

			//Print control values
//			if(chooseprint == 1) {
				printf("\033[2J\033[50A");
				printf("Horizontal Thrusters:\t%d\t%d\t%d\t%d\n", gpio[1], pwm[1], gpio[2], pwm[2]);
				printf("\t\t\t%d\t%d\t%d\t%d\n", gpio[3], pwm[3], gpio[4], pwm[4]);
				printf("Vertical Thrusters:\t%d\t%d\n", gpio[0], pwm[0]);
				printf("Camera Position:\t%d\n", pwm[5]);
				printf("LED Levels:\t\t%d\t%d\n", gpio[6], pwm[6]);
//			}
		}
	}

	close(SockIn);
	close(SockOut);

	exit(0);
}

void BroadcastSend(char *SendString)
{
	SendStringLen = strlen(SendString);	//Find length of sendString
	//Broadcast sendString in datagram to client
	if (sendto(SockOut, SendString, SendStringLen, 0, (struct sockaddr *) &BroadcastAddrOut, sizeof(BroadcastAddrOut)) != SendStringLen)
		printf("sendto() sent a different number of bytes than expected\n");

	//NOT REACHED
}
